package exam;

class StrThread extends Thread
{
    String name;

    public StrThread(String name)
    {
        this.name = name;
    }


    public void run()
    {
        for (int i=1; i<=10; i++)
        {
            try
            {
                Thread.sleep(4000);
                System.out.println(name + " - " + i);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}

public class Main
{
    public static void main(String[] args)
    {

        StrThread StrThread1 = new StrThread("StrThread1");
        StrThread StrThread2 = new StrThread("StrThread2");
        StrThread1.start();
        StrThread2.start();
    }
}